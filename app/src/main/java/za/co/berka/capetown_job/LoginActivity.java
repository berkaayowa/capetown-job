package za.co.berka.capetown_job;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import org.json.JSONException;
import java.util.ArrayList;

import za.co.berka.capetown_job.controllers.job.JobApiCallBack;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.services.JobService;
import za.co.berka.capetown_job.views.ListingJobsActivity;
import za.co.berka.capetown_job.views.RegistrationActivity;

public class LoginActivity extends AppCompatActivity {
    private JobService jobService;
    private boolean isBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

    }

    public void register(View view) {
        //Intent viewHome = new Intent(this, RegistrationActivity.class);
       // viewHome.putExtra("open","tv");
       // this.startActivity(viewHome);
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        // [END subscribe_topics]

        // Log and toast
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("Token-......:", token);

    }

    public void signin(View view) {
        Intent viewHome = new Intent(this, ListingJobsActivity.class);
        viewHome.putExtra("open","tv");
        this.startActivity(viewHome);

    }

    public ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            JobService.MyLocalBinder binder = (JobService.MyLocalBinder) service;
            jobService = binder.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };
    @Override
    protected  void onStop()
    {
        super.onStop();

        if(isBound)
        {
            unbindService(serviceConnection);
            isBound = false;
        }

    }


}
