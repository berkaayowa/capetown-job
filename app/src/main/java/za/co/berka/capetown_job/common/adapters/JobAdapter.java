package za.co.berka.capetown_job.common.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;

import za.co.berka.capetown_job.R;
import za.co.berka.capetown_job.common.Display;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.repository.tables.jobtable.JobInterestedTable;
import za.co.berka.capetown_job.views.JobViewActivity;

import static android.R.attr.country;

/**
 * Created by berka on 3/8/2017.
 */

public class JobAdapter extends ArrayAdapter<Job> {

    private ArrayList<Job> jobs;
    private final Activity context;
    private JobInterestedTable jobInterestedTable;

    public JobAdapter(Activity context, ArrayList<Job> jobs) {
        super(context, R.layout.jobs_layout, jobs);
        this.context = context;
        this.jobs = jobs;
        this.jobInterestedTable = JobInterestedTable.getInstance(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.jobs_layout,null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtTitle);
        TextView txtSubTitle = (TextView) rowView.findViewById(R.id.txtSubTitle);
        TextView txtSalary = (TextView) rowView.findViewById(R.id.txtSalary);
        TextView txtDescription = (TextView) rowView.findViewById(R.id.txtDescription);
        TextView txtTime = (TextView) rowView.findViewById(R.id.txtTime);

        ImageView imgVisited = (ImageView) rowView.findViewById(R.id.imgVisited);
        final ImageView imgSave = (ImageView) rowView.findViewById(R.id.imgSave);


        txtTitle.setText(jobs.get(position).getTitle());
        txtSubTitle.setText(jobs.get(position).getLocation().getCity().getName());
        txtSalary.setText("R " + String.valueOf(jobs.get(position).getSalary()));
        txtDescription.setText(jobs.get(position).getDescription());
        txtTime.setText(jobs.get(position).getPostDate());

        final Job currentJob = getCurrentJob(position);

        if (jobInterestedTable.existInList(currentJob))
        {
            imgSave.setImageResource(R.drawable.likered);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewDetails = new Intent(context, JobViewActivity.class);
                viewDetails.putExtra("type", "job");
                viewDetails.putExtra("job", currentJob);
                context.startActivity(viewDetails);
            }
        });

        imgVisited.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);

                if (jobInterestedTable.existInList(currentJob))
                {
                    if(jobInterestedTable.removeFromList(currentJob))
                    {
                        imgSave.startAnimation(fadeIn);
                        imgSave.setImageResource(R.drawable.like);
                        Display.toast("Job Removed from list", context);
                    }
                    else {
                        Display.toast("Error removing...job", context);
                    }
                }
                else if (jobInterestedTable.save(currentJob))
                {
                    imgSave.startAnimation(fadeIn);
                    imgSave.setImageResource(R.drawable.likered);
                    Display.toast("Job saved", context);
                }
                else
                {
                    Display.toast("Error job", context);
                }

            }
        });


        return rowView;
    }

    public Job getCurrentJob(int position) {
        return jobs.get(position);
    }

}
