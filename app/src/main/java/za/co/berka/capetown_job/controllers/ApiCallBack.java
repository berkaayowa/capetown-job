package za.co.berka.capetown_job.controllers;

import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by berka on 3/8/2017.
 */

public interface ApiCallBack {
    void onConnectingError(VolleyError error);
    void onJSONParsingError(JSONException error);
}
