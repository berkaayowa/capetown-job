package za.co.berka.capetown_job.controllers.job;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.ArrayList;

import za.co.berka.capetown_job.controllers.ApiCallBack;
import za.co.berka.capetown_job.domain.Job;

/**
 * Created by berka on 3/8/2017.
 */

public interface JobApiCallBack extends ApiCallBack {
    void onSuccess(ArrayList<Job> jobs);
}
