package za.co.berka.capetown_job.controllers.user;

import java.util.ArrayList;

import za.co.berka.capetown_job.controllers.ApiCallBack;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.domain.user.User;

/**
 * Created by berka on 3/8/2017.
 */

public interface UserApiCallBack extends ApiCallBack {
    void onSuccess(ArrayList<User> users                         );
}
