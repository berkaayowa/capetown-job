package za.co.berka.capetown_job.controllers.user;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import za.co.berka.capetown_job.common.settings.AppConf;
import za.co.berka.capetown_job.controllers.job.JobApiCallBack;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.domain.jobproperties.JobCategory;
import za.co.berka.capetown_job.domain.jobproperties.JobLocation;
import za.co.berka.capetown_job.domain.jobproperties.JobType;
import za.co.berka.capetown_job.domain.location.City;
import za.co.berka.capetown_job.domain.location.Country;
import za.co.berka.capetown_job.factories.Factory;

/**
 * Created by berka on 2/27/2017.
 */

public class UserController {

    private RequestQueue requestQueue;

    public UserController(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    public void request(final JobApiCallBack callBack) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(AppConf.JoBsListUrl,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jobArray) {
                        try {
                            callBack.onSuccess(convertJobsFromJSON(jobArray));
                        } catch (JSONException e) {
                            callBack.onJSONParsingError(e);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", "ERROR");
                        callBack.onConnectingError(error);
                        Log.e("VOLLEY", error.getMessage());
                    }
                }
        );
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    public void searchRequest(final JobApiCallBack callBack, String keyword) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(AppConf.JoBsSearchUrl + keyword,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jobArray) {
                        try {
                            callBack.onSuccess(convertJobsFromJSON(jobArray));
                        } catch (JSONException e) {
                            callBack.onJSONParsingError(e);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", "ERROR");
                        callBack.onConnectingError(error);
                        Log.e("VOLLEY", error.getMessage());
                    }
                }
        );

        requestQueue.add(jsonObjectRequest);
    }

    public ArrayList<Job> convertJobsFromJSON(JSONObject jobObject) throws JSONException {
        ArrayList<Job>  jobs = new ArrayList<>();
        JSONArray jsonArray = jobObject.getJSONArray("jobs");
        Job job = null;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            long jobId = Long.parseLong(jsonObject.getString("job_id"));
            String jobTitle = jsonObject.getString("job_title");
            String jobDescription = jsonObject.getString("job_description");
            double jobSalary = Double.parseDouble(jsonObject.getString("job_salary"));
            String jobDatePosted = jsonObject.getString("job_date_posted");
            String jobExperience = jsonObject.getString("job_experience");

            JobType jobType = extractJobTypeFromJson(jsonObject.getJSONObject("type"));
            JobCategory jobCategory = extractJobCategoryFromJson(jsonObject.getJSONObject("category"));
            JobLocation jobLocation = extractJobLocationFromJson(jsonObject.getJSONObject("location"));

            job = Factory.createJobOject(jobId, jobTitle, jobDescription, jobExperience,jobDatePosted);
            job.setSalary(jobSalary);
            job.setType(jobType);
            job.setCategory(jobCategory);
            job.setLocation(jobLocation);
            jobs.add(job);
        }

        return jobs;
    }

    private JobType extractJobTypeFromJson(JSONObject jsonObject) {

            try {
                long jobTypeId = Long.parseLong(jsonObject.getString("job_type_id"));
                String jobTypeName = jsonObject.getString("type_name");

                return Factory.createJobTypeObject(jobTypeId ,jobTypeName, "");

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("Convert Type error", e.getMessage());
            }
            finally {

            }

       return null;
    }

    private JobCategory extractJobCategoryFromJson(JSONObject jsonObject) {

            try {
                long jobCatId = Long.parseLong(jsonObject.getString("job_category_id"));
                String jobCatName = jsonObject.getString("category_name");
                String jobCatDescription = jsonObject.getString("category_description");

                return Factory.createCategoryObject(jobCatId, jobCatName, jobCatDescription);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("Convert Type error", e.getMessage());
            }
            finally {

            }

        return null;
    }

    private JobLocation extractJobLocationFromJson(JSONObject jsonObject) {

            try {
                long jobCityId = Long.parseLong(jsonObject.getString("job_city_id"));
                long jobCountryId = Long.parseLong(jsonObject.getString("country_id"));
                String jobCityDescription = jsonObject.getString("city_name");
                String jobCityName = jsonObject.getString("city_name");

                City city = Factory.createCityObject(jobCityId,jobCityName, jobCityDescription);
                Country country = Factory.createCountryObject(jobCountryId, "", "");

                return Factory.createLocationObject(city, country);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("Convert Type error", e.getMessage());
            }
            finally {

            }

        return null;
    }
}
