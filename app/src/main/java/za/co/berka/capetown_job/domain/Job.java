package za.co.berka.capetown_job.domain;

import java.io.Serializable;

import za.co.berka.capetown_job.domain.jobproperties.JobCategory;
import za.co.berka.capetown_job.domain.jobproperties.JobLocation;
import za.co.berka.capetown_job.domain.jobproperties.JobType;

/**
 * Created by berka on 2/27/2017.
 */

public class Job implements Serializable {
    private long id;
    private String title;
    private String description;
    private String experience;
    private String postDate;
    private double salary;

    private JobType type;
    private JobCategory category;
    private JobLocation location;

    public Job() {
    }

    public Job(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Job(long id, String title, String description, String experience, String postDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.experience = experience;
        this.postDate = postDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public JobCategory getCategory() {
        return category;
    }

    public void setCategory(JobCategory category) {
        this.category = category;
    }

    public JobLocation getLocation() {
        return location;
    }

    public void setLocation(JobLocation location) {
        this.location = location;
    }
}
