package za.co.berka.capetown_job.domain.jobproperties;

import java.io.Serializable;

import za.co.berka.capetown_job.domain.location.City;
import za.co.berka.capetown_job.domain.location.Country;

/**
 * Created by berka on 2/27/2017.
 */

public class JobLocation implements Serializable {
    private City city;
    private Country country;

    public JobLocation(City city) {
        this.city = city;
    }

    public JobLocation(City city, Country country) {
        this.city = city;
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
