package za.co.berka.capetown_job.domain.location;

import java.io.Serializable;

/**
 * Created by berka on 2/27/2017.
 */

public class Country implements Serializable {

    private String name;
    private long id;
    private String description;

    public Country() {
    }

    public Country(long id, String name, String description) {
        this.name = name;
        this.id = id;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
