package za.co.berka.capetown_job.domain.user;

import za.co.berka.capetown_job.domain.user.person.Person;

/**
 * Created by berka on 3/28/2017.
 */

public class User extends Person{
    public User(Long id, String name, String surname) {
        super(id, name, surname);
    }
}
