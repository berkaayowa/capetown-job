package za.co.berka.capetown_job.domain.user.contact;

/**
 * Created by berka on 3/28/2017.
 */

public class Address {

    private Long id;
    private String street, suburbub, province, city, country;

    public Address(Long id, String suburbub, String street) {
        this.id = id;
        this.suburbub = suburbub;
        this.street = street;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuburbub() {
        return suburbub;
    }

    public void setSuburbub(String suburbub) {
        this.suburbub = suburbub;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
