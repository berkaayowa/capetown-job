package za.co.berka.capetown_job.factories;

import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.domain.jobproperties.JobCategory;
import za.co.berka.capetown_job.domain.jobproperties.JobLocation;
import za.co.berka.capetown_job.domain.jobproperties.JobType;
import za.co.berka.capetown_job.domain.location.City;
import za.co.berka.capetown_job.domain.location.Country;

/**
 * Created by berka on 3/10/2017.
 */

public class Factory {

    public static Job createJobOject(long id, String title, String description, String experience, String postedDate) {
        Job newJob = new Job(id, title, description, experience, postedDate);
        return newJob;
    }

    public static JobCategory createCategoryObject(long id, String name, String description) {
        JobCategory newCategory = new JobCategory(id, name, description);
        return newCategory;
    }

    public static JobType createJobTypeObject(long id, String name, String description) {
        JobType newType = new JobType(id, name, description);
        return newType;
    }

    public static City createCityObject(long id, String name, String description) {
        City newCity = new City(id, name, description);
        return newCity;
    }

    public static Country createCountryObject(long id, String name, String description) {
        Country newCountry = new Country(id, name, description);
        return newCountry;
    }

    public static JobLocation createLocationObject(City city, Country country) {
        JobLocation newJobLocation = new JobLocation(city, country);
        return newJobLocation;
    }

    public static JobLocation createLocationObject(City city) {
        JobLocation newJobLocation = new JobLocation(city);
        return newJobLocation;
    }
}
