package za.co.berka.capetown_job.repository.tables.apptable;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import za.co.berka.capetown_job.common.Converter;

/**
 * Created by berka on 3/12/2017.
 */

public class AppTable extends SQLiteOpenHelper {

    private SQLiteDatabase sqlLiteDatabase;
    private ContentValues contentValues;
    private String tableName;
    private ArrayList<Attribute> attributes;

    public AppTable(String databaseName, int version, String tableName, ArrayList<Attribute> attributes, Activity context) {
        super(context, databaseName, null,  version);
        this.tableName = tableName;
        this.attributes = attributes;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            final String query = Converter.toCreateTableQuery(this.tableName, this.attributes);
            db.execSQL(query);
        }
        catch (Exception ex) {
            Log.d("SQL ERROR", ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + this.tableName + ";");
        onCreate(sqLiteDatabase);
    }

    protected boolean add(ArrayList<Attribute> attributes) {
        this.sqlLiteDatabase = this.getWritableDatabase();
        this.contentValues = new ContentValues();

        long returned;
        int attrSize = attributes.size();

        if (attrSize > 0)
        {
            for (int i = 0 ;i < attrSize; i++) {
                this.contentValues.put(attributes.get(i).name, attributes.get(i).value);
            }

            try {
                returned = this.sqlLiteDatabase.insert(this.tableName, null, this.contentValues);
            }catch (Exception ex) {
                returned = -1;
                Log.d("exception ::add::",ex.getMessage());
            }

            if(returned != -1)
            {
                return true;
            }
        }
        return false;
    }

    protected boolean update() {
        return true;
    }

    protected boolean delete(Attribute attribute) {
        long returned ;
        this.sqlLiteDatabase = this.getWritableDatabase();

        try {

            returned =  this.sqlLiteDatabase.delete(this.tableName,
                    attribute.name+ " = ?",
                    new String[]{attribute.value});

        } catch (Exception ex) {
            Log.d("exception ::delete::",ex.getMessage());
            returned = 0;
        }

        return (returned != 0) ? true : false;
    }

    protected boolean existIn(Attribute attribute) {
        this.sqlLiteDatabase = this.getReadableDatabase();
        String query = Converter.toSelectAllWhere(this.tableName,
                attribute, String.valueOf(attribute.value));
        Cursor data = this.sqlLiteDatabase.rawQuery(query, null);

        if(data.getCount() != 0) {

        }
        return (data.getCount() > 0) ? true : false;
    }

    protected ArrayList<TableRow> fetchAll(ArrayList<Attribute> attributes) {

        int length = attributes.size();
        ArrayList<TableRow> table = new ArrayList<>();
        this.sqlLiteDatabase = this.getReadableDatabase();
        String query = Converter.toSelectAll(this.tableName);

        Cursor data =  this.sqlLiteDatabase.rawQuery(query, null);

        if(data.getCount() != 0) {
            while (data.moveToNext()) {

                for (int i = 0; i < length;i++) {
                    int currentColIndex = data.getColumnIndex(attributes.get(i).name);
                    if (attributes.get(i).name.equalsIgnoreCase(data.getColumnName(currentColIndex)))
                    {
                        //data.getType()
                        switch (attributes.get(i).type)
                        {
                            case "Text":
                                attributes.get(i).setValue(data.getString(currentColIndex));
                                break;
                            default:
                                attributes.get(i).setValue(String.valueOf(data.getInt(currentColIndex)));
                                break;
                        }

                        break;
                    }
                }

                TableRow row = new TableRow(attributes);
                table.add(row);
            }
        }
        else
        {
            table = null;
        }

        return table;
    }
}
