package za.co.berka.capetown_job.repository.tables.apptable;

/**
 * Created by Berka on 9/26/2016.
 */
public class Attribute {
    public String name;
    public String type;
    public String value;

    public Attribute(String name, String type) {
        this.name = name;
        this.type = type;
        this.value = "0";
    }

    public void setValue(String value) {
        this.value = value;
    }

}
