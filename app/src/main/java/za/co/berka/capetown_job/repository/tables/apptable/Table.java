package za.co.berka.capetown_job.repository.tables.apptable;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by Berka on 9/26/2016.
 */
public abstract class Table extends AppTable {

        public Table(String databaseName, int version, String tableName, ArrayList<Attribute> attributes, Activity context) {
                super(databaseName, version, tableName, attributes, context);
        }

        protected abstract String getTableName();
        protected abstract Attribute getPrimaryKey();

}

