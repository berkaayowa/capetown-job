package za.co.berka.capetown_job.repository.tables.apptable;

import java.util.ArrayList;

/**
 * Created by berka on 3/14/2017.
 */

public class TableRow {
    private ArrayList<Attribute> attributes;

    public TableRow(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    public ArrayList<Attribute> getAttributes() {
        return this.attributes;
    }
}
