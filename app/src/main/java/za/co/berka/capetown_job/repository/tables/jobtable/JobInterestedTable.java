package za.co.berka.capetown_job.repository.tables.jobtable;


import android.app.Activity;

import java.util.ArrayList;

import za.co.berka.capetown_job.common.settings.AppConf;
import za.co.berka.capetown_job.common.settings.Database;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.domain.jobproperties.JobLocation;
import za.co.berka.capetown_job.domain.location.City;
import za.co.berka.capetown_job.factories.Factory;
import za.co.berka.capetown_job.repository.tables.apptable.Attribute;
import za.co.berka.capetown_job.repository.tables.apptable.Table;
import za.co.berka.capetown_job.repository.tables.apptable.TableRow;

/**
 * Created by Berka on 9/26/2016.
 */
public class JobInterestedTable extends Table{

    private static String tableName = AppConf.name + "interested";

    //Table Attributes declaration
    private static Attribute id = new Attribute("id", "number");
    private static Attribute name = new Attribute("name", "Text");
    private static Attribute cityId = new Attribute("cityId", "number");
    private static Attribute city = new Attribute("city", "Text");
    private static Attribute salary = new Attribute("salary", "Text");
    private static Attribute experience = new Attribute("experience", "Text");
    private static Attribute description = new Attribute("description", "Text");
    private static Attribute postedDate = new Attribute("postedDate", "Text");

    private static JobInterestedTable singleton = null;

    private JobInterestedTable(Activity context) {
        super(Database.name, Database.version, tableName, getAttributes(), context);
    }

    //Table singleton object
    public static JobInterestedTable getInstance(Activity context)
    {
        if (singleton == null) {
            singleton = new JobInterestedTable(context);
        }
        return singleton;
    }

    @Override
    protected String getTableName() {
        return tableName;
    }

    @Override
    protected Attribute getPrimaryKey() {
        return id;
    }

    /*Adding all attributes to one row
      Array list of attributes
    */

    private static ArrayList<Attribute> getAttributes() {
        ArrayList<Attribute> attributes = new ArrayList<>();
        attributes.add(id);
        attributes.add(name);
        attributes.add(cityId);
        attributes.add(city);
        attributes.add(salary);
        attributes.add(experience);
        attributes.add(description);
        attributes.add(postedDate);
        return attributes;
    }

    public boolean save(Job job) {
        id.setValue(String.valueOf(job.getId()));
        name.setValue(job.getTitle());
        cityId.setValue(String.valueOf(job.getLocation().getCity().getId()));
        city.setValue(job.getLocation().getCity().getName());
        salary.setValue(String.valueOf(job.getSalary()));
        experience.setValue(job.getExperience());
        description.setValue(job.getDescription());
        postedDate.setValue(job.getPostDate());
        return this.add(getAttributes());
    }

    public boolean removeFromList(Job job) {
        id.setValue(String.valueOf(job.getId()));
        return this.delete(id);
    }

    public boolean existInList(Job job) {
        id.setValue(String.valueOf(job.getId()));
        return this.existIn(id);
    }

    public  ArrayList<Job> getSavedJob() {
        ArrayList<TableRow> jobsFromDb = this.fetchAll(getAttributes());
        ArrayList<Job> jobs = new ArrayList<>();

        int tableLength = jobsFromDb.size();
        for (int i = 0; i < tableLength; i++) {

            for (int y = 0; y < jobsFromDb.get(i).getAttributes().size(); y++) {
                switch (y) {
                    case 0:
                        this.id.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 1:
                        this.name.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 2:
                        this.cityId.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 3:
                        this.city.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 4:
                        this.salary.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 5:
                        this.experience.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 6:
                        this.description.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                    case 7:
                        this.postedDate.value = jobsFromDb.get(i).getAttributes().get(y).value;
                        break;
                }

                City city= Factory.createCityObject(Long.parseLong(this.cityId.value),
                        this.city.value, "");

                Job job = Factory.createJobOject(Long.parseLong(this.id.value),this.name.value,
                        this.description.value, this.experience.value, this.postedDate.value);
                job.setLocation(Factory.createLocationObject(city));

                jobs.add(job);

            }
        }
        return jobs;
    }

}
