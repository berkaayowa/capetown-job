package za.co.berka.capetown_job.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.ArrayList;

import za.co.berka.capetown_job.controllers.job.JobApiCallBack;
import za.co.berka.capetown_job.controllers.job.JobController;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.repository.tables.jobtable.JobInterestedTable;

public class JobService extends Service {
    private final IBinder myBinder = new MyLocalBinder ( ) ;
    private ArrayList<Job> jobs;
    private JobController jobController;
    private JobInterestedTable jobInterestedTable;
    public JobService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public class MyLocalBinder extends Binder {
        public JobService getService() {
            jobController = new JobController(getApplicationContext());
            return JobService.this;
        }
    }

    public String test() {
        return "works";
    }

    public void getJobs(final JobApiCallBack callBack) {

        jobController.request(new JobApiCallBack() {

            @Override
            public void onSuccess(ArrayList<Job> jobs) {
                callBack.onSuccess(jobs);
            }

            @Override
            public void onConnectingError(VolleyError error) {
                callBack.onConnectingError(error);
            }

            @Override
            public void onJSONParsingError(JSONException error) {
                callBack.onJSONParsingError(error);
            }
        });
    }

    public void searchJobs(final JobApiCallBack callBack, String keyword) {

        jobController.searchRequest(new JobApiCallBack() {

            @Override
            public void onSuccess(ArrayList<Job> jobs) {
                callBack.onSuccess(jobs);
            }

            @Override
            public void onConnectingError(VolleyError error) {
                callBack.onConnectingError(error);
            }

            @Override
            public void onJSONParsingError(JSONException error) {
                callBack.onJSONParsingError(error);
            }
        }, keyword);
    }

}
