package za.co.berka.capetown_job.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import za.co.berka.capetown_job.R;
import za.co.berka.capetown_job.domain.Job;

public class JobViewActivity extends AppCompatActivity {
    private TextView txtTitle;
    private TextView txtSubTitle;
    private TextView txtSalary;
    private TextView txtDescription;
    private TextView txtExperience;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_view);

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtSubTitle = (TextView) findViewById(R.id.txtSubTitle);
        txtSalary = (TextView) findViewById(R.id.txtSalary);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtExperience = (TextView) findViewById(R.id.txtExperience);

        Bundle extras = getIntent().getExtras();
        this.getDataFromBundle(extras);
    }

    public void getDataFromBundle(Bundle extras) {
        if (extras.get("type").equals("job")) {
            Job job = (Job) getIntent().getSerializableExtra("job");
            txtTitle.setText(job.getTitle());
            txtSubTitle.setText(job.getLocation().getCity().getName());
            txtSalary.setText("R " + String.valueOf(job.getSalary()));
            txtDescription.setText(job.getDescription());
            txtExperience.setText(job.getExperience());
            Log.e("error------>" ,String.valueOf(job.getSalary()));
            this.setTitle(job.getTitle());
        }
    }
}
