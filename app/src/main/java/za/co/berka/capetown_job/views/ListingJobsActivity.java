package za.co.berka.capetown_job.views;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.VolleyError;
import org.json.JSONException;
import java.util.ArrayList;

import za.co.berka.capetown_job.R;
import za.co.berka.capetown_job.common.Display;
import za.co.berka.capetown_job.common.adapters.JobAdapter;
import za.co.berka.capetown_job.controllers.job.JobApiCallBack;
import za.co.berka.capetown_job.domain.Job;
import za.co.berka.capetown_job.services.JobService;


public class ListingJobsActivity extends AppCompatActivity {
    private ListView jobListView;
    private JobService jobService;
    private boolean isBound;
    private SearchView searchView;
    private TextView txtResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_jobs);
        jobListView = (ListView) findViewById(R.id.JobListView);
        Display.startLoading("Loading jobs", this);

        txtResults = (TextView) findViewById(R.id.txtResults);

        isBound = false;
        bindService(new Intent(this, JobService.class), serviceConnection, Context.BIND_AUTO_CREATE);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String keyword) {
               // Display.toast(s, getApplicationContext());
                searchJob(keyword);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //Display.toast("changed", getApplicationContext());
                return false;
            }
        });
        return true;
    }

    public void openView(View view) {
        //Intent viewHome = new Intent(this, JobViewActivity.class);
       // viewHome.putExtra("open","tv");
        //this.startActivity(viewHome);
       // Display.toast("message",getApplicationContext());
    }

    public void loadJobList(ArrayList<Job> jobs) {
        txtResults.setText("Found : " + String.valueOf(jobs.size()) + " Jobs" );
        JobAdapter adapter = new JobAdapter(this,jobs);
        jobListView.setAdapter(adapter);
        Display.endLoading();
    }

    public void displayJobs() {

        jobService.getJobs(new JobApiCallBack() {
            @Override
            public void onSuccess(ArrayList<Job> jobs) {
                loadJobList(jobs);
            }

            @Override
            public void onConnectingError(VolleyError error) {
                Log.e("ERROR-VOLEY:" ,error.getMessage());
            }

            @Override
            public void onJSONParsingError(JSONException error) {
                Log.e("ERROR-JSON:" ,error.getMessage());
            }
        });

    }

    public void searchJob(String keyword) {
        Display.startLoading("Searching jobs", this);
        jobService.searchJobs(new JobApiCallBack() {
            @Override
            public void onSuccess(ArrayList<Job> jobs) {
                loadJobList(jobs);
            }

            @Override
            public void onConnectingError(VolleyError error) {
                Log.e("ERROR-VOLEY:" ,error.getMessage());
            }

            @Override
            public void onJSONParsingError(JSONException error) {
                Log.e("ERROR-JSON:" ,error.getMessage());
            }
        }, keyword);
    }

    public ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            JobService.MyLocalBinder binder = (JobService.MyLocalBinder) service;
            jobService = binder.getService();
            isBound = true;

            if (isBound)
            {
                displayJobs();
            }
            else
            {
                Log.e("ERROR->>>>>:" ,"no bound");
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };
    @Override
    protected  void onStop()
    {
        super.onStop();

        if(isBound)
        {
            unbindService(serviceConnection);
            isBound = false;
        }

    }

    public void alert(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (this).getLayoutInflater();
       // builder.setTitle("");
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setCancelable(true);
        //builder.setIcon(R.drawable.ap);
        builder.setView(inflater.inflate(R.layout.job_filter_layout, null));

        builder.create();
        builder.show();
    }
}
